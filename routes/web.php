<?php

use App\Http\Controllers\KendaraanController;
use App\Http\Controllers\SukuCadangController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('dashboard');
});


Route::get('/kendaraan', [KendaraanController::class, 'index'])->name('kendaraan.index');
Route::get('/kendaraan-table', [KendaraanController::class, 'table'])->name('kendaraan.table');
Route::get('/suku-cadang', [SukuCadangController::class, 'index'])->name('suku.cadang.index');
Route::get('/suku-cadang-table', [SukuCadangController::class, 'table'])->name('suku.cadang.table');
Route::get('/kendaraan/create', [KendaraanController::class, 'create'])->name('kendaraan.create');
Route::get('/suku-cadang/create', [SukuCadangController::class, 'create'])->name('suku.cadang.create');
Route::get('/kendaraan/{id}/edit', [KendaraanController::class, 'edit'])->name('kendaraan.edit');
Route::get('/kendaraan/{id}/show', [KendaraanController::class, 'show'])->name('kendaraan.show');
Route::get('/kendaraan/{id}/table-suku-cadang', [KendaraanController::class, 'tableSukuCadang'])->name('kendaraan.table.suku.cadang');
Route::get('/suku-cadang/{id}/edit', [SukuCadangController::class, 'edit'])->name('suku.cadang.edit');

Route::post('/kendaraan/store', [KendaraanController::class, 'store'])->name('kendaraan.store');
Route::post('/suku-cadang/store', [SukuCadangController::class, 'store'])->name('suku.cadang.store');

Route::put('/kendaraan/{id}/update', [KendaraanController::class, 'update'])->name('kendaraan.update');
Route::put('/kendaraan/{id}/update-suku-cadang', [KendaraanController::class, 'updateSukuCadang'])->name('kendaraan.update.suku.cadang');
Route::put('/suku-cadang/{id}/update', [SukuCadangController::class, 'update'])->name('suku.cadang.update');

Route::delete('/kendaraan/{id}/delete', [KendaraanController::class, 'destroy'])->name('kendaraan.delete');
Route::delete('/kendaraan/{id}/delete-suku-cadang', [KendaraanController::class, 'destroySukuCadang'])->name('kendaraan.delete.suku.cadang');
Route::delete('/suku-cadang/{id}/delete', [SukuCadangController::class, 'destroy'])->name('suku.cadang.delete');
