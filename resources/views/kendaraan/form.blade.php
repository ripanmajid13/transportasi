@extends('app')

@section('content')
    <h1 class="mt-5">@if ($model->id) Ubah @else Tambah @endif Kendaraan</h1>

    @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <form action="{{ $action }}" method="POST">
        @csrf
        @if ($model->id) @method('PUT') @endif

        <div class="row mb-5">
            <div class="col-4">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{ $model->nama ?? old('nama') }}">
                @error('nama')
                    <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-4">
                <label for="plat" class="form-label">Plat</label>
                <input type="text" class="form-control" name="plat" id="plat" value="{{ $model->plat ?? old('plat') }}">
                @error('plat')
                    <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-4">
                <label for="jenis" class="form-label">Jenis</label>
                <input type="text" class="form-control" name="jenis" id="jenis" value="{{ $model->jenis ?? old('jenis') }}">
                @error('jenis')
                    <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="d-flex justify-content-center">
            <a href="{{ route('kendaraan.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn @if ($model->id) btn-warning @else btn-primary @endif mx-2">Simpan</button>
            @if (!$model->id)
                <button type="reset" class="btn btn-danger">Ulangi</button>
            @endif
        </div>
    </form>
@endsection
