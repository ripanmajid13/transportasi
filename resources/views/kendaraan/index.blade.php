@extends('app')

@push('script_inline')
    <script>
        let dataTable = $("#dataTable").DataTable({
            "responsive": true,
            "autoWidth": false,
            processing: true,
            serverSide: true,
            ajax: "{{ $urlTable }}",
            columns: [
                {data: "DT_RowIndex", class: "align-middle"},
                {data: "nama", class: "align-middle"},
                {data: "plat", class: "align-middle"},
                {data: "jenis", class: "align-middle"},
                {data: "action", class: "align-middle text-center"},
            ],
            columnDefs: [
                { responsivePriority: 1, targets: 1 },
                { responsivePriority: 2, targets: -1 },
            ],
            responsive: {
                details: {
                    renderer: function ( api, rowIdx, columns ) {
                        var data = $.map( columns, function ( col, i ) {
                            if (col.title.length == 0) {
                                var title = 'Action',
                                    padding = 'class="pt-2 pb-1"';
                            } else {
                                var title = col.title,
                                    padding = 'class="pt-1 pb-1"';
                            }
                            return col.hidden ?
                                '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'" class="text-sm">'+
                                    '<td '+padding+'>'+title+' <span class="float-right">:</span>'+'</td> '+
                                    '<td class="pt-1 pb-1">'+col.data+'</td>'+
                                '</tr>' :
                                '';
                        } ).join('');
                        return data ?$('<table/>').append( data ) :false;
                    }
                }
            },
            pageLength: 10,
            lengthMenu: [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
            "language": {
                "emptyTable": '<p class="my-3">Tidak ada data yang tersedia pada tabel ini</p>'
            }
        });

        $('body').on('click', '.btn-delete', function(e) {
            e.preventDefault();

            let me = $(this),
                url = me.attr('href'),
                text = me.data().text,
                notif = confirm(`Hapu ${text} ?`)

            if (notif) {
                $.post(url, {
                    "_method" : "DELETE",
                    "_token" : $('meta[name="csrf-token"]').attr('content')
                }, (res) => {
                    dataTable.ajax.reload()
                    $('#alert-success').show().find('.text-content').html(res)
                })
                .fail((xhr) => alert(`${text} gagal dihapus.`));

            }
        })
    </script>
@endpush

@section('content')
    <h1 class="mt-5">Kendaraan</h1>
    <a href="{{ route('kendaraan.create') }}" class="btn btn-primary mb-2">Tambah</a>
    <div id="alert-success" class="alert alert-success alert-dismissible fade show" role="alert" style="display: none">
        <p class="mb-0 text-content"></p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <table id="dataTable" class="table table-sm table-bordered table-hover text-xs dt-responsive nowrap mb-0" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Plat</th>
                <th>Jenis</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td colspan="6" class="py-5">&nbsp;</td>
            </tr>
        </tbody>
    </table>
@endsection
