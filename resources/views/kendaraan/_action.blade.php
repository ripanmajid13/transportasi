<a href="{{ $show }}" class="btn btn-info btn-sm">Detail</a>
<a href="{{ $edit }}" class="btn btn-warning btn-sm">Ubah</a>
<a href="{{ $delete }}" class="btn btn-danger btn-sm btn-delete" data-text="{{ $text }}">Hapus</a>
