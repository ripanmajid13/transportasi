@extends('app')

@push('script_inline')
    <script>
        $("#dataTable").DataTable({
            "responsive": true,
            "autoWidth": false,
            processing: true,
            serverSide: true,
            ajax: "{{ $urlTable }}",
            columns: [
                {data: "DT_RowIndex", class: "align-middle"},
                {data: "suku_cadang", class: "align-middle"},
                {data: "jumlah", class: "align-middle"},
                {data: "action", class: "align-middle text-center"},
            ],
            columnDefs: [
                { responsivePriority: 1, targets: 1 },
                { responsivePriority: 2, targets: -1 },
            ],
            responsive: {
                details: {
                    renderer: function ( api, rowIdx, columns ) {
                        var data = $.map( columns, function ( col, i ) {
                            if (col.title.length == 0) {
                                var title = 'Action',
                                    padding = 'class="pt-2 pb-1"';
                            } else {
                                var title = col.title,
                                    padding = 'class="pt-1 pb-1"';
                            }
                            return col.hidden ?
                                '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'" class="text-sm">'+
                                    '<td '+padding+'>'+title+' <span class="float-right">:</span>'+'</td> '+
                                    '<td class="pt-1 pb-1">'+col.data+'</td>'+
                                '</tr>' :
                                '';
                        } ).join('');
                        return data ?$('<table/>').append( data ) :false;
                    }
                }
            },
            pageLength: 10,
            lengthMenu: [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
            "language": {
                "emptyTable": '<p class="my-3">Tidak ada data yang tersedia pada tabel ini</p>'
            }
        });
    </script>
@endpush

@section('content')
    <div class="card mt-5">
        <div class="card-header">
            <h1>Detail Kendaraan</h1>
        </div>

        <div class="card-body">
            <h5 class="card-title">{{ $model->jenis }} {{ $model->nama }} {{ $model->plat }}</h5>

            <hr />

            <p class="card-text text-center text-lg">Suku Cadang yang sudah digunakan.</p>

            @if (Session::get('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ Session::get('warning') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            @if (Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ Session::get('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif


            <div class="card mb-3">
                <div class="card-header">
                    Form Tambah Suku Cadang
                </div>
                <div class="card-body px-2 py-2">
                    <form class="row" action="{{ $urlSukuCadang }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-auto d-flex flex-column justify-content-start">
                            <select class="form-select" name="suku_cadang" aria-label="Default select">
                                <option selected value="" disabled>Silahkan Pilih Suku Cadang</option>
                                @foreach ($sukuCadang as $sk)
                                    <option value="{{ $sk->id }}" {{ (old("suku_cadang") == $sk->id ? "selected" : "") }}>{{ $sk->nama }}</option>
                                @endforeach
                            </select>
                            @error('suku_cadang')
                                <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="col-auto d-flex flex-column justify-content-start">
                            <input type="text" class="form-control" name="jumlah" placeholder="Jumlah" value="{{ old('jumlah') }}">
                            @error('jumlah')
                                <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="col-auto">
                          <button type="submit" class="btn btn-primary">Tambah</button>
                          <a href="{{ route('kendaraan.index') }}" class="btn btn-secondary">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>

            <table id="dataTable" class="table table-sm table-bordered table-hover text-xs dt-responsive nowrap mb-0" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Suku Cadang</th>
                        <th>Jumlah</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td colspan="6" class="py-5">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
