<form action="{{ $batal }}" method="POST">
    @csrf
    @method('DELETE')

    <button type="submit" class="btn btn-danger btn-sm">Batal</button>
</form>
