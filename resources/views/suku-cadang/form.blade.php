@extends('app')

@section('content')
    <h1 class="mt-5">@if ($model->id) Ubah @else Tambah @endif Suku Cadang</h1>

    @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <form action="{{ $action }}" method="POST">
        @csrf
        @if ($model->id) @method('PUT') @endif

        <div class="row mb-5">
            <div class="col-6">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{ $model->nama ?? old('nama') }}">
                @error('nama')
                    <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-6">
                <label for="jumlah" class="form-label">Jumlah</label>
                <input type="text" class="form-control" name="jumlah" id="jumlah" value="{{ $model->jumlah ?? old('jumlah') }}">
                @error('jumlah')
                    <div class="alert alert-danger" style="background-color: transparent; border-color: transparent; padding: 0">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="d-flex justify-content-center">
            <a href="{{ route('suku.cadang.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn @if ($model->id) btn-warning @else btn-primary @endif mx-2">Simpan</button>
            @if (!$model->id)
                <button type="reset" class="btn btn-danger">Ulangi</button>
            @endif
        </div>
    </form>
@endsection
