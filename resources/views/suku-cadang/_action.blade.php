@if ($digunakan)
    <button type="button" class="btn btn-secondary btn-sm">Ubah</button>
    <button type="button" class="btn btn-secondary btn-sm">Hapus</button>
@else
    <a href="{{ $edit }}" class="btn btn-warning btn-sm">Ubah</a>
    <a href="{{ $delete }}" class="btn btn-danger btn-sm btn-delete" data-text="{{ $text }}">Hapus</a>
@endif
