<?php

namespace App\Http\Controllers;

use App\Models\SukuCadang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SukuCadangController extends Controller
{
    public function index()
    {
        return view('suku-cadang/index', [
            'urlTable' => route('suku.cadang.table')
        ]);
    }

    public function create()
    {
        return view('suku-cadang.form', [
            'model'         => new SukuCadang,
            'action'        => route('suku.cadang.store'),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'   => ['required', 'unique:suku_cadang'],
            'jumlah' => ['required', 'numeric'],
        ], [
            'nama.required'   => 'Nama harus diisi.',
            'nama.unique'     => 'Nama '.$request->nama.' sudah ada',
            'jumlah.required' => 'Jumlah harus diisi.',
            'jumlah.numeric'  => 'Jumlah harus berisikan angka.'
        ]);

        $model         = new SukuCadang;
        $model->kode   = 'SC'.Carbon::now()->format('YmdHis');
        $model->nama   = $request->nama;
        $model->jumlah = $request->jumlah;
        $model->save();

        return redirect()->back()->with('success','Suku Cadang '.$request->nama.' berhasil disimpan.');
    }

    public function edit($id)
    {
        $model = SukuCadang::findOrFail($id);

        if ($model->kendaraans->count()) {
            return redirect()->back()->with('danger', 'Suku Cadang '.$model->kode.' tidak bisa diubah.');
        } else {
            return view('suku-cadang.form', [
                'model'         => $model,
                'action'        => route('suku.cadang.update', $id),
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'   => ['required', 'unique:suku_cadang,nama,'.$id],
            'jumlah' => ['required', 'numeric'],
        ], [
            'nama.required'   => 'Nama harus diisi.',
            'nama.unique'     => 'Nama '.$request->nama.' sudah ada',
            'jumlah.required' => 'Jumlah harus diisi.',
            'jumlah.numeric'  => 'Jumlah harus berisikan angka.'
        ]);

        $model         = SukuCadang::findOrFail($id);
        $model->nama   = $request->nama;
        $model->jumlah = $request->jumlah;
        $model->save();

        return redirect()->back()->with('success','Suku Cadang '.$request->nama.' berhasil diperbaharui.');
    }

    public function destroy($id)
    {
        $model = SukuCadang::findOrFail($id);

        if ($model->kendaraans->count()) {
            return response()->json([
                'sts' => 'danger',
                'msg' => 'Suku Cadang '.$model->kode.' tidak bisa dihapus.'
            ]);
        } else {
            $model->delete();

            return response()->json([
                'sts' => 'success',
                'msg' => 'Kode Suku Cadang '.$model->kode.' berhasil dihapus.'
            ]);
        }
    }

    public function table()
    {
        $model = SukuCadang::orderBy('nama', 'asc')->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('suku-cadang._action', [
                    'digunakan' => $model->kendaraans->count(),
                    'text' => 'Suku Cadang dengan kode '.$model->kode,
                    'edit' => route('suku.cadang.edit', $model->id),
                    'delete' => route('suku.cadang.delete', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
