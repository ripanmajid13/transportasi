<?php

namespace App\Http\Controllers;

use App\Models\Kendaraan;
use App\Models\SukuCadang;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class KendaraanController extends Controller
{
    public function index()
    {
        return view('kendaraan.index', [
            'urlTable' => route('kendaraan.table')
        ]);
    }

    public function create()
    {
        return view('kendaraan.form', [
            'model'         => new Kendaraan(),
            'action'        => route('kendaraan.store'),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'  => ['required'],
            'plat'  => ['required', 'unique:kendaraan'],
            'jenis' => ['required'],
        ], [
            'nama.required'  => 'Nama harus diisi.',
            'plat.required'  => 'Plat harus diisi.',
            'plat.unique'    => 'Plat '.$request->nama.' sudah ada',
            'jenis.required' => 'Jenis harus diisi.',
        ]);

        $model        = new Kendaraan();
        $model->nama  = $request->nama;
        $model->plat  = $request->plat;
        $model->jenis = $request->jenis;
        $model->save();

        return redirect()->back()->with('success', $request->jenis.' '.$request->nama.' '.$request->plat.' berhasil disimpan.');
    }

    public function show($id)
    {
        return view('kendaraan.show', [
            'model' => Kendaraan::findOrFail($id),
            'sukuCadang' => SukuCadang::get(),
            'urlSukuCadang' => route('kendaraan.update.suku.cadang', $id),
            'urlTable' => route('kendaraan.table.suku.cadang', $id)
        ]);
    }

    public function edit($id)
    {
        return view('kendaraan.form', [
            'model'         => Kendaraan::findOrFail($id),
            'action'        => route('kendaraan.update', $id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'  => ['required'],
            'plat'  => ['required', 'unique:kendaraan,plat,'.$id],
            'jenis' => ['required'],
        ], [
            'nama.required'  => 'Nama harus diisi.',
            'plat.required'  => 'Plat harus diisi.',
            'plat.unique'    => 'Plat '.$request->nama.' sudah ada',
            'jenis.required' => 'Jenis harus diisi.',
        ]);

        $model        = Kendaraan::findOrFail($id);
        $model->nama  = $request->nama;
        $model->plat  = $request->plat;
        $model->jenis = $request->jenis;
        $model->save();

        return redirect()->back()->with('success', $request->jenis.' '.$request->nama.' '.$request->plat.' berhasil diperbaharui.');
    }

    public function updateSukuCadang(Request $request, $id)
    {
        $request->validate([
            'suku_cadang'  => ['required'],
            'jumlah' => ['required'],
        ], [
            'suku_cadang.required'  => 'Suku Cadang harus dipilih.',
            'jumlah.required'  => 'Jumlah harus diisi.',
        ]);

        $sk = SukuCadang::findOrFail($request->suku_cadang);
        $total = $sk->jumlah - $sk->kendaraans->pluck('pivot')->pluck('jumlah')->sum();

        if ($total < $request->jumlah) {
            return redirect()->back()->withInput()->with('warning', 'Stock Suku Cadang '.$sk->nama.' hanya tersisa '.$total.'.');
        } else {
            $sk->kendaraans()->attach($id, ['jumlah' => $request->jumlah]);

            return redirect()->back()->with('success', 'Suku Cadang '.$sk->nama.' berhasil ditambahkan.');
        }
    }


    public function destroy($id)
    {
        $model = Kendaraan::findOrFail($id);
        $model->delete();
        $model->suku_cadangs()->detach();
        return response()->json('Kendaraan berhasil dihapus');
    }

    public function destroySukuCadang(Request $request, $id)
    {
        $model = SukuCadang::findOrFail($id);
        $model->kendaraans()->wherePivot('id', $request->pivot_id)->detach();


        return redirect()->back()->with('success', 'Suku Cadang '.$model->nama.' berhasil dihapus.');
    }

    public function table()
    {
        $model = Kendaraan::orderBy('nama', 'asc')->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('kendaraan._action', [
                    'text' => 'Modil dengan plat '.$model->plat,
                    'show' => route('kendaraan.show', $model->id),
                    'edit' => route('kendaraan.edit', $model->id),
                    'delete' => route('kendaraan.delete', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function tableSukuCadang($id)
    {
        $model = Kendaraan::findOrFail($id)->suku_cadangs;
        return DataTables::of($model)
            ->addColumn('suku_cadang', function ($model) {
                return SukuCadang::findOrFail($model->pivot->suku_cadang_id)->nama;
            })
            ->addColumn('jumlah', function ($model) {
                return $model->pivot->jumlah;
            })
            ->addColumn('digunakan', function ($model) {
                return $model->pivot->digunakan ? 'Sudah' : 'Belum';
            })
            ->addColumn('action', function ($model) {
                return view('kendaraan._action_suku_cadang', [
                    'batal' => route('kendaraan.delete.suku.cadang', [
                        'id'       => $model->pivot->suku_cadang_id,
                        'pivot_id' => $model->pivot->id,
                        'kendaraan_id' => $model->id
                    ]),
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
