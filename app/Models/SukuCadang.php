<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SukuCadang extends Model
{
    use HasFactory;

    protected $table = 'suku_cadang';

    public function kendaraans()
    {
        return $this->belongsToMany(Kendaraan::class, 'kendaraan_suku_cadang')
        ->withPivot('id', 'jumlah', 'digunakan');
    }
}
