<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    use HasFactory;

    protected $table = 'kendaraan';

    public function suku_cadangs()
    {
        return $this->belongsToMany(SukuCadang::class, 'kendaraan_suku_cadang')
            ->withPivot('id', 'jumlah', 'digunakan')
            ->orderBy('kendaraan_suku_cadang.digunakan', 'asc');
    }
}
